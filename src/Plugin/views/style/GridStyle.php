<?php

namespace Drupal\sbr_views\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Style plugin for the grid view.
 *
 * @ViewsStyle(
 *   id = "grid_style",
 *   title = @Translation("Grid Style"),
 *   help = @Translation("Displays content in grid."),
 *   theme = "sbr_views_grid_style",
 *   display_types = {"normal"}
 * )
 */
class GridStyle extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['gap'] = ['default' => TRUE];
    $options['columns'] = ['default' => '1'];
    return $options;
  }


  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['gap'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Gutters between grid rows and columns'),
      '#default_value' => !empty($this->options[TRUE]),
    ];
    $form['border'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Applica un bordo intorno alla card'),
      '#default_value' => !empty($this->options[FALSE]),
    ];
    
    $form['columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of columns'),
      '#default_value' => $this->options['columns'],
      '#required' => TRUE,
      '#min' => 1,
    ];
    
  }
}
